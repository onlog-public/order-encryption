package orderencryption

import (
	"github.com/golang-jwt/jwt/v5"
)

// Encrypt принимает объект расчета стоимости, и возвращает шифрованный токен доступа.
func Encrypt(calculation jwt.Claims, encryptionKey string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, calculation)

	return token.SignedString([]byte(encryptionKey))
}
