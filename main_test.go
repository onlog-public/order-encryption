package orderencryption_test

import (
	"github.com/golang-jwt/jwt/v5"
	orderencryption "gitlab.com/onlog-public/order-encryption"
	"testing"
)

type testS struct {
	ID string `json:"id"`
}

type Service struct {
	jwt.RegisteredClaims

	Id    []testS `json:"ids"`
	Price float64 `json:"price"`
}

func Test_Encrypt(t *testing.T) {
	serviceData := Service{
		Id: []testS{
			{ID: "ttt"},
		},
		Price: 345.67,
	}

	key := "rteejto22eif2.df.dwe240-gsv2p[344993j"

	tokenString, err := orderencryption.Encrypt(serviceData, key)
	if err != nil {
		t.Error(err)
	}

	err = orderencryption.Validate[Service](tokenString, key, serviceData, func(a, b Service) bool {
		return a.Price == b.Price
	})
	if err != nil {
		t.Error(err)
	}
}
