package orderencryption

import (
	"github.com/golang-jwt/jwt/v5"
	"github.com/pkg/errors"
)

// Validate валидирует данные зашифрованные в token, и пришедшие в data, c помощью функции validateFn.
func Validate[T jwt.Claims](token string, encryptionKey string, data T, validateFn func(a, b T) bool) (err error) {
	defer func() { err = errors.Wrap(err, `[order-encryption::Validate]`) }()

	unencryptedData, err := Decrypt[T](token, encryptionKey)
	if err != nil {
		return
	}

	ok := validateFn(unencryptedData, data)
	if !ok {
		err = errors.New("failed data validate")
	}

	return
}
