package orderencryption

import (
	"fmt"
	"gitlab.systems-fd.com/packages/golang/helpers/h"

	"github.com/golang-jwt/jwt/v5"
	"github.com/pkg/errors"
)

// Decrypt расшифровывает tokenString, и возвращает объект расчета стоимости.
func Decrypt[T jwt.Claims](tokenString string, encryptionKey string) (T, error) {
	var err error
	defer func() { err = errors.Wrap(err, `[order-encryption::Decrypt]`) }()

	var result T
	res := h.ToInterface(h.ToPointer(result)).(jwt.Claims)

	token, err := jwt.ParseWithClaims(tokenString, res, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", t.Header["alg"])
		}
		return []byte(encryptionKey), nil
	})
	if err != nil {
		return result, err
	}

	parsedData, ok := h.ToInterface(token.Claims).(*T)
	if !ok {
		return result, errors.New(`token type and result type are incompatible`)
	}

	return *parsedData, nil
}
