module gitlab.com/onlog-public/order-encryption

go 1.21.1

require (
	github.com/golang-jwt/jwt/v5 v5.0.0
	github.com/pkg/errors v0.9.1
)

require (
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	gitlab.systems-fd.com/packages/golang/helpers/h v0.16.0
)
